﻿using PlatformService.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace PlatformService.Data
{
    public class PrepDb
    {
        public static void PrepPopulation(IApplicationBuilder app,bool isProd)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                SeedData(serviceScope.ServiceProvider.GetService<AppDbContext>(),isProd);
            }
        }
        private static void SeedData(AppDbContext context,bool isProd)
        {
            if (isProd)
            {
                Console.WriteLine("attempting to  migrate");
                try
                {
                    context.Database.Migrate();
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Could not run migrateions{ex.Message}");
                }
            }
            if (!context.Platforms.Any())
            {
                Console.WriteLine("---Seeding data");
                context.Platforms.AddRange(
                    new Platform() { Cost ="Free",Id = 0,Name="Dot Net",Publisher="Microsoft"},
                    new Platform() { Cost ="Free",Id = 0,Name="SQL Server Expres",Publisher="Microsoft"},
                    new Platform() { Cost ="Free",Id = 0,Name="Kubernates",Publisher="Cloud Native Computing FOundation"}
                    );
                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Data is already has");
            }

        }
    }
}
