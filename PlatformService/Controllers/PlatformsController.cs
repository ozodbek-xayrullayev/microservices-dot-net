﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PlatformService.AsyncDataServices;
using PlatformService.Data;
using PlatformService.DTOs;
using PlatformService.Models;
using PlatformService.SyncDataServices.Http;

namespace PlatformService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlatformsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IPlatformRepo _repository;
        private readonly ICommandDataClient _commandDataClient;
        private readonly IMessageBusClient _messageBusClient;
        public PlatformsController(
            IPlatformRepo repository,
            IMapper mapper,
            ICommandDataClient commandDataClient,
            IMessageBusClient messageBusClient)
        {
            _mapper = mapper;
            _repository = repository;
            _commandDataClient = commandDataClient;
            _messageBusClient = messageBusClient;
        }
        [HttpGet]
        public ActionResult<IEnumerable<PlatformReadDTO>> GetPlatforms()
        {
            var platformItems = _repository.GetAllPlatforms();
            return Ok(_mapper.Map<IEnumerable<PlatformReadDTO>>(platformItems));

        }
        [HttpGet("{id}", Name = "GetPlatformById")]
        public ActionResult<PlatformReadDTO> GetPlatformById(int id)
        {
            var platformItem = _repository.GetPlatformById(id);
            if(platformItem !=null) return Ok(_mapper.Map<IEnumerable<PlatformReadDTO>>(platformItem));
            return NotFound();
        }
        [HttpPost]
        public async Task<ActionResult<PlatformReadDTO>> CreatePlatform(PlatformCreateDTO platformCreateDTO)
        {
            var platformModel = _mapper.Map<Platform>(platformCreateDTO);
            _repository.CreatePlatform(platformModel);
            _repository.SaveChanges();
            var platformReadDTO = _mapper.Map<PlatformReadDTO>(platformModel);
            //SendSync message
            try
            {
                await _commandDataClient.SendPlatformToCommand(platformReadDTO);

            }
            catch (Exception ex)    
            {
                Console.WriteLine($"---> Could not send synchro {ex.Message}");
            }
            //send Async message
            try
            {
                var platformPublishDTO=_mapper.Map<PlatformPublishedDto>(platformReadDTO);
                platformPublishDTO.Event = "Platform_Published";
                _messageBusClient.PublishNewPlatform(platformPublishDTO);
            }
            catch(Exception ex)
            {

            }
            return CreatedAtRoute(nameof(GetPlatformById), new { Id = platformReadDTO.Id }, platformReadDTO);
        }
    }
}
